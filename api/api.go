package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"last-exam/online_market_go_api_gateaway/api/docs"
	"last-exam/online_market_go_api_gateaway/api/handlers"
	"last-exam/online_market_go_api_gateaway/config"
)

// New
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func SetUpAPI(r *gin.Engine, h handlers.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	// @securityDefinitions.apikey ApiKeyAuth
	// @in header
	// @name Authorization

	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(5000))

	r.GET("/config", h.GetConfig)

	r.POST("/login", h.Login)
	r.POST("/sign_up", h.SignUp)

	r.POST("/user", h.CreateUser)
	r.GET("/user/:id", h.GetUserByID)
	r.GET("/user", h.GetUserList)
	r.PUT("/user/:id", h.UpdateUser)
	r.PATCH("/user/:id", h.UpdatePatchUser)
	r.DELETE("/user/:id", h.DeleteUser)

	r.POST("/product", h.CreateProduct)
	r.GET("/product/:id", h.GetProductByID)
	r.GET("/product", h.GetProductList)
	r.GET("/user_product", h.GetUserProductList)
	r.PUT("/product/:id", h.UpdateProduct)
	r.PATCH("/product/:id", h.UpdatePatchProduct)
	r.DELETE("/product/:id", h.DeleteUser)

	r.POST("/order", h.CreateOrder)
	r.GET("/order/:id", h.GetOrderById)
	r.GET("/order", h.GetOrderList)
	r.PUT("/order/:id", h.UpdateOrder)
	r.PATCH("/order/:id", h.UpdatePatchOrder)
	r.DELETE("/order/:id", h.DeleteOrder)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}
