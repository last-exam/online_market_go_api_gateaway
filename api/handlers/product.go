package handlers

import (
	"context"
	"fmt"

	"github.com/gin-gonic/gin"

	"last-exam/online_market_go_api_gateaway/api/http"
	"last-exam/online_market_go_api_gateaway/api/models"
	"last-exam/online_market_go_api_gateaway/genproto/product_service"
	"last-exam/online_market_go_api_gateaway/pkg/helper"
	"last-exam/online_market_go_api_gateaway/pkg/util"
)

// CreateProduct godoc
// @ID create_Product
// @Router /product [POST]
// @Summary Create Product
// @Description  Create Product
// @Tags Product
// @Accept json
// @Produce json
// @Param profile body product_service.CreateProduct true "CreateProductRequestBody"
// @Success 200 {object} http.Response{data=product_service.Product} "GetProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateProduct(c *gin.Context) {

	var product product_service.CreateProduct

	err := c.ShouldBindJSON(&product)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ProductService().CreateProductFunc(
		c.Request.Context(),
		&product,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetProductByID godoc
// @ID get_Product_by_id
// @Router /product/{id} [GET]
// @Summary Get Product  By ID
// @Description Get Product  By ID
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=product_service.Product} "ProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetProductByID(c *gin.Context) {

	productID := c.Param("id")

	if !util.IsValidUUID(productID) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	resp, err := h.services.ProductService().GetByID(
		context.Background(),
		&product_service.ProductPrimaryKey{
			Id: productID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetProductList godoc
// @ID get_Product_list
// @Router /product [GET]
// @Summary Get Product s List
// @Description  Get Product s List
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param message_status query string false "message_status"
// @Success 200 {object} http.Response{data=product_service.GetListProductResponse} "GetAllProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetProductList(c *gin.Context) {

	ownerID := c.Param("id")

	fmt.Println("::::", ownerID)

	// if !util.IsValidUUID(ownerID) {
	// 	h.handleResponse(c, http.InvalidArgument, "owner id is an invalid uuid")
	// 	return
	// }

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	fmt.Println("222", offset, limit)

	resp, err := h.services.ProductService().GetList(
		context.Background(),
		&product_service.GetListProductRequest{
			Limit:         int32(limit),
			Offset:        int32(offset),
			Search:        c.Query("search"),
			MessageStatus: c.Query("message_status"),
			OwnerId:       ownerID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetProductList godoc
// @ID get_product_list
// @Router /user_product [GET]
// @Summary Get Product s List
// @Description  Get Product s List
// @Tags Product
// @Accept json
// @Produce json
// @Param id query string false "id"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param message_status query string false "message_status"
// @Success 200 {object} http.Response{data=product_service.GetListProductResponse} "GetAllProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetUserProductList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ProductService().GetList(
		context.Background(),
		&product_service.GetListProductRequest{
			Limit:         int32(limit),
			Offset:        int32(offset),
			Search:        c.Query("search"),
			MessageStatus: c.Query("message_status"),
			OwnerId:       c.Query("id"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateProduct godoc
// @ID update_product
// @Router /product/{id} [PUT]
// @Summary Update Product
// @Description Update Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body product_service.UpdateProduct true "UpdateProductRequestBody"
// @Success 200 {object} http.Response{data=product_service.Product} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateProduct(c *gin.Context) {

	var product product_service.UpdateProduct

	product.Id = c.Param("id")

	if !util.IsValidUUID(product.Id) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&product)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ProductService().UpdateProductFunc(
		c.Request.Context(),
		&product,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchProduct godoc
// @ID patch_product
// @Router /product/{id} [PATCH]
// @Summary Patch Product
// @Description Patch Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=product_service.Product} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchProduct(c *gin.Context) {

	var updatePatchProduct models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchProduct.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchProduct.ID) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchProduct.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ProductService().UpdatePatchProductFunc(
		c.Request.Context(),
		&product_service.UpdatePatchProduct{
			Id:     updatePatchProduct.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteProduct godoc
// @ID delete_product
// @Router /product/{id} [DELETE]
// @Summary Delete Product
// @Description Delete Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteProduct(c *gin.Context) {

	ProductId := c.Param("id")

	if !util.IsValidUUID(ProductId) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	resp, err := h.services.ProductService().DeleteProduct(
		c.Request.Context(),
		&product_service.ProductPrimaryKey{Id: ProductId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
