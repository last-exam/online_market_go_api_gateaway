package handlers

import (
	"github.com/gin-gonic/gin"

	"last-exam/online_market_go_api_gateaway/api/http"
	"last-exam/online_market_go_api_gateaway/genproto/user_service"
)

// Login godoc
// @ID login
// @Router /login [POST]
// @Summary Login
// @Description  Login
// @Tags Auth
// @Accept json
// @Produce json
// @Param login body user_service.UserAuthorize true "LoginRequestBody"
// @Success 200 {object} http.Response{data=user_service.User} "GetLoginBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) Login(c *gin.Context) {

	var login user_service.UserAuthorize

	err := c.ShouldBindJSON(&login)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.UserService().UserAuthorization(
		c.Request.Context(),
		&login,
	)

	if err != nil {
		h.handleResponse(c, http.NoContent, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// Login godoc
// @ID sign_up
// @Router /sign_up [POST]
// @Summary SignUp
// @Description  SignUp
// @Tags Auth
// @Accept json
// @Produce json
// @Param login body user_service.CreateUser true "SignUpRequestBody"
// @Success 200 {object} http.Response{data=user_service.User} "GetLoginBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) SignUp(c *gin.Context) {

	var sign_up user_service.CreateUser

	err := c.ShouldBindJSON(&sign_up)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.UserService().Create(
		c.Request.Context(),
		&sign_up,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}
