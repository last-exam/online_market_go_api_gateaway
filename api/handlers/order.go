package handlers

import (
	"context"
	"fmt"

	"github.com/gin-gonic/gin"

	"last-exam/online_market_go_api_gateaway/api/http"
	"last-exam/online_market_go_api_gateaway/api/models"
	"last-exam/online_market_go_api_gateaway/genproto/order_service"
	"last-exam/online_market_go_api_gateaway/genproto/user_service"
	"last-exam/online_market_go_api_gateaway/pkg/helper"
	"last-exam/online_market_go_api_gateaway/pkg/util"
)

// CreateOrder godoc
// @ID create_order
// @Router /order [POST]
// @Summary Create Order
// @Description  Create Order
// @Tags Order
// @Accept json
// @Produce json
// @Param profile body order_service.CreateOrder true "CreateOrderRequestBody"
// @Success 200 {object} http.Response{data=order_service.Order} "GetOrderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateOrder(c *gin.Context) {

	var order order_service.CreateOrder

	err := c.ShouldBindJSON(&order)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().Create(
		c.Request.Context(),
		&order,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetOrderById godoc
// @ID get_order_by_id
// @Router /order/{id} [GET]
// @Summary Get Order  By ID
// @Description Get Order  By ID
// @Tags Order
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param userid query string false "userid"
// @Success 200 {object} http.Response{data=order_service.Order} "OrderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetOrderById(c *gin.Context) {

	orderId := c.Param("id")

	userId := c.Query("userid")

	resp, err := h.services.OrderService().GetByID(
		context.Background(),
		&order_service.OrderPrimaryKey{
			Id:     orderId,
			UserId: userId,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetOrderList godoc
// @ID get_order_list
// @Router /order [GET]
// @Summary Get Order s List
// @Description  Get Order s List
// @Tags Order
// @Accept json
// @Produce json
// @Param user_id query string false "user_id"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=order_service.GetListOrderResponse} "GetAllOrderResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetOrderList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.OrderService().GetList(
		context.Background(),
		&order_service.GetListOrderRequest{
			Limit:  int32(limit),
			Offset: int32(offset),
			Search: c.Query("search"),
			UserId: c.Query("user_id"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateOrder godoc
// @ID update_order
// @Router /order/{id} [PUT]
// @Summary Update Order
// @Description Update Order
// @Tags Order
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body order_service.UpdateOrder true "UpdateOrderRequestBody"
// @Success 200 {object} http.Response{data=order_service.Order} "Order data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateOrder(c *gin.Context) {

	var order order_service.UpdateOrder

	order.Id = c.Param("id")

	if !util.IsValidUUID(order.Id) {
		h.handleResponse(c, http.InvalidArgument, "order id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&order)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().Update(
		c.Request.Context(),
		&order,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchOrder godoc
// @ID patch_order
// @Router /order/{id} [PATCH]
// @Summary Patch Order
// @Description Patch Order
// @Tags Order
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=order_service.Order} "Order data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchOrder(c *gin.Context) {

	var updatePatchUser models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchUser)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchUser.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchUser.ID) {
		h.handleResponse(c, http.InvalidArgument, "order id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchUser.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.OrderService().UpdatePatch(
		c.Request.Context(),
		&order_service.UpdatePatchOrder{
			Id:     updatePatchUser.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteOrder godoc
// @ID delete_order
// @Router /order/{id} [DELETE]
// @Summary Delete Order
// @Description Delete Order
// @Tags Order
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Order data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteOrder(c *gin.Context) {

	userId := c.Param("id")

	if !util.IsValidUUID(userId) {
		h.handleResponse(c, http.InvalidArgument, "order id is an invalid uuid")
		return
	}

	resp, err := h.services.UserService().Delete(
		c.Request.Context(),
		&user_service.UserPrimaryKey{Id: userId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
