package services

import (
	"last-exam/online_market_go_api_gateaway/genproto/product_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"last-exam/online_market_go_api_gateaway/config"
	"last-exam/online_market_go_api_gateaway/genproto/order_service"
	"last-exam/online_market_go_api_gateaway/genproto/user_service"
)

type ServiceManagerI interface {
	UserService() user_service.UserServiceClient
	OrderService() order_service.OrderServiceClient
	ProductService() product_service.ProductServiceClient
}

type grpcClients struct {
	userService    user_service.UserServiceClient
	orderService   order_service.OrderServiceClient
	productService product_service.ProductServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// User Service...
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Product Service...
	connProductService, err := grpc.Dial(
		cfg.ProductServiceHost+cfg.ProductGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Product Service...
	connOrderService, err := grpc.Dial(
		cfg.OrderServiceHost+cfg.OrderGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		userService:    user_service.NewUserServiceClient(connUserService),
		orderService:   order_service.NewOrderServiceClient(connOrderService),
		productService: product_service.NewProductServiceClient(connProductService),
	}, nil
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}

func (g *grpcClients) OrderService() order_service.OrderServiceClient {
	return g.orderService
}

func (g *grpcClients) ProductService() product_service.ProductServiceClient {
	return g.productService
}
