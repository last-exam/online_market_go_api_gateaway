package main

import (
	"fmt"

	"github.com/gin-gonic/gin"

	"last-exam/online_market_go_api_gateaway/api"
	"last-exam/online_market_go_api_gateaway/api/handlers"
	"last-exam/online_market_go_api_gateaway/config"
	"last-exam/online_market_go_api_gateaway/pkg/logger"
	"last-exam/online_market_go_api_gateaway/services"
)

func main() {
	cfg := config.Load()

	fmt.Printf("config: %+v\n", cfg)

	grpcSvcs, err := services.NewGrpcClients(cfg)
	if err != nil {
		panic(err)
	}

	var loggerLevel = new(string)

	*loggerLevel = logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		*loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		*loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		*loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger("uu_go_api_gateway", *loggerLevel)
	defer func() {
		err := logger.Cleanup(log)
		if err != nil {
			return
		}
	}()

	r := gin.New()

	r.Use(gin.Logger(), gin.Recovery())

	h := handlers.NewHandler(cfg, log, grpcSvcs)

	api.SetUpAPI(r, h, cfg)

	fmt.Println("Start api gateway....")

	if err := r.Run(cfg.HTTPPort); err != nil {
		return
	}
}
